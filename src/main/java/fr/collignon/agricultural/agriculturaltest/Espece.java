package fr.collignon.agricultural.agriculturaltest;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class Espece {

    @Id
    @GeneratedValue(strategy= GenerationType.AUTO)
    private int idEspece = 1;
    private String nom;

    public String getNom() {
        return nom;
    }

    public int getIdEspece() {
        return idEspece;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public void setIdEspece(int idEspece) {
        this.idEspece = idEspece;
    }
}
