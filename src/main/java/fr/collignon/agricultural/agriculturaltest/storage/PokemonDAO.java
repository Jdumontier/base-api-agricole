package fr.collignon.agricultural.agriculturaltest.storage;

import fr.collignon.agricultural.agriculturaltest.Pokemon;

import java.util.List;

public interface PokemonDAO {

    public Iterable<Pokemon> getMyPokemons();
    public void insererPokemon(Pokemon pokemon);
}
