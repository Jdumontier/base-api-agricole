package fr.collignon.agricultural.agriculturaltest.storage;

import fr.collignon.agricultural.agriculturaltest.Pokemon;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface PokemonRepository extends CrudRepository<Pokemon, Integer> {
}
