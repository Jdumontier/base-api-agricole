package fr.collignon.agricultural.agriculturaltest;

import fr.collignon.agricultural.agriculturaltest.storage.PokemonDAODatabase;
import fr.collignon.agricultural.agriculturaltest.storage.PokemonRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.boot.test.mock.mockito.MockBean;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

@SpringBootTest(classes = PokemonDAODatabase.class)
public class PokemonDAODatabaseTest {

    @Autowired
    private PokemonDAODatabase pokemonDAODatabase;

    @MockBean
    private PokemonRepository pokemonRepository;

    @Test
    public void shouldReturnNullInCaseOfError() {
        // GIVEN
        Mockito.when(pokemonRepository.findAll()).thenThrow(new RuntimeException());

        // WHEN
        List<Pokemon> pokemons = pokemonDAODatabase.getPokemonsTries();

        // THEN
        Assertions.assertNull(pokemons);
    }

    @Test
    public void shouldReturnSortedPokemons() {
        // GIVEN
        // TODO : faire une fausse liste
        Mockito.when(pokemonRepository.findAll()).thenReturn(new ArrayList<>());

        // WHEN
        List<Pokemon> pokemons = pokemonDAODatabase.getPokemonsTries();

        // THEN
        // assert pokemons tries (éventuellement en utilisant assertj)
    }

    public void setPokemonDAODatabase(PokemonDAODatabase pokemonDAODatabase) {
        this.pokemonDAODatabase = pokemonDAODatabase;
    }

    public PokemonDAODatabase getPokemonDAODatabase() {
        return pokemonDAODatabase;
    }
}
